$(document).ready(function () {

    // Preloader
    setTimeout(function () {
        $('#loader-wrapper').fadeOut();
    }, 2000);

    // Navbar change color :: Only affected to the index page
    $(document).scroll(function () {
        var $nav = $(".transparent-nav");
        $nav.toggleClass('scrolled', $(this).scrollTop() > $nav.height());
    });

    // Typed headline
    var type = new Typed('#main-text', {
        strings: ['Hello!', 'We are <span class="lead-text-bg">Synergy Creative Agency!</span> ', 'We are here to <span class="lead-text-bg">help you!</span>', 'Meet Our <span class="lead-text-bg">Agency!</span>'],
        typeSpeed: 80,
        backSpeed: 50,
        fadeOut: true,
        loop: true,
        showCursor: false
    });

    // Smooth scrolling
    (function ($) {
        "use strict";
        $('a.scroll-trigger[href*="#"]:not([href="#"])').click(function () {
            if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
                var target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                if (target.length) {
                    $('html, body').animate({
                        scrollTop: (target.offset().top - 54)
                    }, 1000, "easeInOutExpo");
                    return false;
                }
            }
        });
    })(jQuery);
    
    // Code...
});
